import * as firebase from 'firebase';
import isotop from './users.js';
import createUserPage from './createUserPage.js';
import addSkills from './addSkills.js';
// import getUserData from './getUserData';
import '../style/index.scss';

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCX7cyC_x7TTI4u-SFUmhJW3CPuDWrem_0",
  authDomain: "skills-d7bba.firebaseapp.com",
  databaseURL: "https://skills-d7bba.firebaseio.com",
  storageBucket: "skills-d7bba.appspot.com"
};
  // projectId: "skills-d7bba",
  // messagingSenderId: "1821600550"

var usersList = document.querySelector('.users');
var app = document.querySelector('.app');

var createUser = (id, user) => {
	var markup = (
		`<div class="user grid-item ${user.skills.primSkill}" data-id=${id}>
			<img class="user__img" data-id=${id} src=${user.url} alt=${user.description}>
			<div class="user__discription" data-id=${id}>
				<p class="user__name" data-id=${id}>${user.name},  ${user.age} years old</p>
				<p class="user__hard-skill" data-id=${id}>${user.skills.primSkill}</p>
			</div>
		</div>`
	);
	return markup;
};
const initUser = (id, user) => {
	usersList.innerHTML += createUser(id, user);
}

const showUserPortfolio = (id) => {
	getUserData(id);
	app.innerHTML = '';
	app.innerHTML += createUserPage(id);
};

const getUserData = (userID) => {
	var userData = firebase.database().ref(`users/${userID}`);

	userData.once("value")
		.then((snapshot) => {
			let { skills : { skillList }, education, experience} = snapshot.val();
			return skillList;			
		}).then((skillList) => {
			addSkills(skillList)
			addEducation(education)
			addExperience(experience)
		})
}


window.onload = () => {

	var db = firebase.initializeApp(config);
	var users = firebase.database().ref();
		
	users.once("value")
		.then((snapshot) => {
			var key = snapshot.child("users").val();
			key.forEach((k, i) => {
				initUser(i, k);
			})
		}).then(()=> {isotop()});

	usersList.addEventListener('click', (e) => {
		e.preventDefault();
		const userID = e.target.getAttribute("data-id");
		if(userID != null){
			showUserPortfolio();
			getUserData(userID);

		} 
	})
};