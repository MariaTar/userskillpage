var newUsersList = [
	{
		'id': 1,
		'name': "Anton Tsyganok",
		'age': 25,
		'skill': {
			'primerySkill': 'UI'
		},
		'url': './style/img.jpg',
		'discription': 'user_picture,Anton_photo'
	},{
		'id': 2,
		'name': "Mariia Tretiak",
		'age': 22,
		'skill': {
			'primerySkill':'JS'
		},
		'url': './style/img.jpg',
		'discription': 'user_picture,Mariia_photo'
	},{
		'id': 3,
		'name': "Mariia Tarasiuk",
		'age': 29,
		'skill': {
			'primerySkill':'UI'
		},
		'url': './style/img.jpg',
		'discription': 'user_picture,Mariia_photo'
	},{
		'id': 4,
		'name': "Daniil Bilokha",
		'age': 21,
		'skill': {
			'primerySkill':'JS'
		},
		'url': './style/img.jpg',
		'discription': 'user_picture,Daniil_photo'
	},{
		'id': 5,
		'name': "Yana Melnyk",
		'age': 27,
		'skill': {
			'primerySkill':'JS'
		},
		'url': './style/img.jpg',
		'discription': "user_picture,Yana_photo",
	}];
export default newUsersList;