export default addSkills = (list) => {

		const skillList = document.getElementById('skill-list');
		list.forEach((skill, index) => {
			skillList.innerHTML += `<li class="skill__item ${skill.skillName}" style='width: ${skill.skillRange}% !important'>${skill.skillName}</li>`
		})
}
