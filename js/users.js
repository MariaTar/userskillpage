import Isotop from 'isotope-layout';

const initIsotop = () => {

	var employeeItems = document.getElementsByClassName('grid-item'),
		filtersElem = document.getElementById('filter');
		
	var	iso = new Isotope('.grid', {
	    itemSelector: '.grid-item',
	    layoutMode: 'fitRows'
	  });

	filtersElem.addEventListener( 'click', function( event ) {

			if(!event.target.matches('a')) return;
			event.preventDefault();
			var filterValue = event.target.getAttribute('data-filter');
	    iso.arrange({filter: filterValue});
	});
}		
export default initIsotop;
