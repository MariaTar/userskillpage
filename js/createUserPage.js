const createUserPage = () => {
	// getUserPortfolio(id);
	const markup = (
		`
		<menu id="sidebar">
			<div class="menu">
				<button class="menu__btn menu__btn_close btn" id='closeBtn'>Close  &times;</button>
				<a href="#skill" class="menu__item">Skill</a>
				<a href="#education" class="menu__item">Education</a>
				<a href="#experience" class="menu__item">Experience</a>
			</div>
			<button class="menu__btn menu__btn_open btn" id="openBtn"><i class="fa fa-navicon"></i></button>
		</menu>


	<main class="main">
		<section class="skill" id="skill">
			<form action="POST" class="skill__form">
				<label class="form__item" for="skillName">Skill name:</label>
				<input type="text" id="skillName" class="skill__input form__item" placeholder="Enter skill name">
				<br>
				<label class="form__item" for="skillRange">Skill range:</label>
				<input type="number" id="skillRange" class="skill__input form__item" min="10" step="10" max="100" placeholder="Enter skill range">
				<br>
				<input type="submit" id="skillBtn" class="btn form__item" value="Add skill">
			</form>
			<div id='skill-list'></div>
		</section>

		<header><h2>Education</h2></header>
		<article id="education" class="education">

			<section class="education__item" >
				<div class="education__time">
					<p class="education__year">2017</p>
				</div>
				<article class="education__description">
					<h3 class="education__title">Title</h3>
					<p class="education__text">Lorem ipsum dolor sit amet, consectetur
						adipisicing elit. Officia provident soluta similique animi dolorum quam,
						alias voluptates mollitia iure aut quis sequi eligendi veritatis non
						fugiat magnam sint id. Esse rerum, quam est aperiam quia assumenda neque
						tempore maiores placeat fugit odio, commodi cumque, impedit enim incidunt
						! Id, quisquam, facere!</p>
				</article>
			</section>

			<section class="education__item" >
				<div class="education__time">
					<p class="education__year">2017</p>
				</div>
				<article class="education__description">
					<h3 class="education__title">Title</h3>
					<p class="education__text">Lorem ipsum dolor sit amet, consectetur
						adipisicing elit. Officia provident soluta similique animi dolorum quam,
						alias voluptates mollitia iure aut quis sequi eligendi veritatis non
						fugiat magnam sint id. Esse rerum, quam est aperiam quia assumenda neque
						tempore maiores placeat fugit odio, commodi cumque, impedit enim incidunt
						! Id, quisquam, facere!</p>
				</article>
			</section>
		</article>

		<header><h2>Experience</h2></header>
		<article id="experience" class="experience">

			<section class="experience__item">
				<div class="experience__time">
					<h3>Company</h3>
					<p>2014-2016</p>
				</div>
				<div class="experience__description">
					<h3 class="experience__position">Position</h3>
					<p class="experience__description">Lorem ipsum dolor sit amet,
						consectetur adipisicing elit. Qui aspernatur tempora, impedit,
						quaerat esse enim dolore. Fugit ipsam, consequatur dolores optio
						deserunt veritatis, necessitatibus ex itaque saepe explicabo neque,
						cupiditate.</p>
				</div>
			</section>

			<section class="experience__item">
				<div class="experience__time">
					<h3>Company</h3>
					<p>2014-2016</p>
				</div>
				<div class="experience__description">
					<h3 class="experience__position">Position</h3>
					<p class="experience__description">Lorem ipsum dolor sit amet,
						consectetur adipisicing elit. Qui aspernatur tempora, impedit,
						quaerat esse enim dolore. Fugit ipsam, consequatur dolores optio
						deserunt veritatis, necessitatibus ex itaque saepe explicabo neque,
						cupiditate.</p>
				</div>
			</section>

		</article>
	</main>
	
	<button class="to-top btn"><i class="fa fa-chevron-up"></i></button>
		`
		);
	return markup;
} 

export default createUserPage;