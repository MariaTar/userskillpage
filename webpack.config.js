const   path = require('path'),
        webpack = require('webpack');
       ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: './js/index.js',
    output: {
        filename: 'main.bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    devServer:{
        contentBase: path.resolve(__dirname, '.')
    },
    devtool: "source-map",
    plugins:[
        new ExtractTextPlugin({
            filename: './style/index.css',
            disable: false,
            allChunks: true
        })],
    module:{
        rules:[{
            loader: 'babel-loader',
            options: {
                presets: ["es2015"]
            }
        }],
        loaders: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loaders: 'babel-loader'
        },
        {
            test: /\.css$/,
            loader: "style-loader!css-loader"
        }
        ]
    },
        resolve: {
        alias: {
            'masonry': 'masonry-layout',
            'isotope': 'isotope-layout'
        }
    }
};